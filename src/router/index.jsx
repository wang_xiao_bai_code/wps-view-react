import React from "react"
import {HashRouter as Router,Route,Switch} from "react-router-dom"
import Index from "../views";
import WebFilePage from "../views/webFile";
import DbFilePage from "../views/dbFile";
import ViewFilePage from "../components/view";
import CreateFilePage from "../components/create";

export default class Routes extends React.Component{
    constructor(props){
        super(props);
        this.state = {}
    }

    render(){
        return(
            <Router>
                <Switch>
                    <Route path="/viewFile" component={ViewFilePage} />
                    <Route path="/createFile" component={CreateFilePage} />
                    <Route path={"/"} render={()=>(
                        <Index>
                            <Route path="/" exact component={WebFilePage} />
                            <Route path="/webFile" component={WebFilePage} />
                            <Route path="/dbFile" component={DbFilePage} />
                        </Index>
                    )}/>
                </Switch>
            </Router>
        )
    }
}
